package com.bca.controller;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;

import adapter.DeviceManagementAdapter;
import adapter.LogAdapter;
import adapter.LoginAdapter;
import adapter.PassbookAdapter;
import model.Globals;

@RestController
public class controller {

	@RequestMapping(value = "/ping",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody void GetPing()
	{
		return;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/loginByCard",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlAPIResult GetLoginCard(@RequestBody model.mdlLoginCard param)
	{
		model.mdlCardDetails cardDetails = new model.mdlCardDetails();

		model.mdlAPIResult mdlGetLoginCardResult = new model.mdlAPIResult();
		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
		model.mdlMessage mdlMessage = new model.mdlMessage();

		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.WSID = param.WSID;
		mdlLog.SerialNumber = param.SerialNumber;
		mdlLog.ApiFunction = "loginByCard";
		mdlLog.SystemFunction = "GetLoginCard";
		mdlLog.LogSource = "Webservice";
		mdlLog.LogStatus = "Failed";

		List<model.mdlAccount> listAccount, listAccountFiltered = new ArrayList<model.mdlAccount>();

		Gson gson = new Gson();
		try{
			if (param.CardNumber == null || param.CardNumber.equals("") || param.PINBlock == null || param.PINBlock.equals("")){
				mdlErrorSchema.ErrorCode = "01";
				mdlMessage.Indonesian = "Nomor kartu atau PIN tidak valid";
				mdlMessage.English = mdlLog.ErrorMessage = "Card Number or PIN is not valid";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
				LogAdapter.InsertLog(mdlLog);
				return mdlGetLoginCardResult;
			}

			//login by card
			//call function to call BCA API to verify account using card number and PIN
			model.mdlAPIResult mdlAPIResult = LoginAdapter.verifyPIN(param);

			if (mdlAPIResult.ErrorSchema == null){
				mdlErrorSchema.ErrorCode = "02";
				mdlMessage.Indonesian = "Gagal memanggil service";
				mdlMessage.English = "Service call failed";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
				return mdlGetLoginCardResult;
			}else if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")){
				mdlErrorSchema.ErrorCode = "03";
	 	    	mdlMessage.Indonesian = mdlAPIResult.ErrorSchema.ErrorMessage.Indonesian;
	 	    	mdlMessage.English = mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
	 	    	mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
				LogAdapter.InsertLog(mdlLog);
				return mdlGetLoginCardResult;
			}

			LinkedTreeMap<String, JsonArray> outputSchemaMap = (LinkedTreeMap<String, JsonArray>) mdlAPIResult.OutputSchema;
			JsonObject mObj = gson.toJsonTree(outputSchemaMap).getAsJsonObject();
			Type collectionType = new TypeToken<model.mdlCardDetails>(){}.getType();
			cardDetails = gson.fromJson( mObj.get("CardDetails").toString() , collectionType);

			//CHECK IF RETURN CAN BE NOT NULL!!!
			if (cardDetails == null){
				mdlErrorSchema.ErrorCode = "04";
				mdlMessage.Indonesian = "Detail kartu kosong";
				mdlMessage.English = mdlLog.ErrorMessage = "Empty card details";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
				LogAdapter.InsertLog(mdlLog);
				return mdlGetLoginCardResult;
			}

			collectionType = new TypeToken<List<model.mdlAccount>>(){}.getType();
			cardDetails.AccountDetails = gson.fromJson( mObj.get("AccountList").toString() , collectionType);
			cardDetails.ResultB24 = gson.fromJson( mObj.get("ResultB24").toString() , String.class);

			//list of mdlAccount from mdlCardDetails
			listAccount = cardDetails.AccountDetails;

			List<String> accountNumber = new ArrayList<String>(); //to contain account number values to be used in get account details function

			//get account number from acoount list and put account number value to a list of string
			for (model.mdlAccount account : listAccount){
				accountNumber.add(account.AccountNumber);
			}
			//set list of acccount number string into single string with comma delimiter ex: "123123,456456,789789"
		    StringJoiner joiner = new StringJoiner(",", "", "");
		    accountNumber.forEach(joiner::add);

			//encode comma value in url parameter to utf8
            //String accountNumberList = URLEncoder.encode(joiner.toString(), "UTF-8");
		    String accountNumberList = joiner.toString();

	 	    //call BCA API get account details by card number
            mdlAPIResult = new model.mdlAPIResult();
            mdlAPIResult = LoginAdapter.getAccountDetailsByCard(accountNumberList, param.SerialNumber, param.WSID);

            if (mdlAPIResult.ErrorSchema == null){
            	mdlErrorSchema.ErrorCode = "09";
		 	    mdlMessage.Indonesian = "Gagal mendapatkan data akun";
		 	    mdlMessage.English = "Get account data failed";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
				return mdlGetLoginCardResult;
            }else if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")){
            	mdlErrorSchema.ErrorCode = "05";
		 	    mdlMessage.Indonesian = mdlAPIResult.ErrorSchema.ErrorMessage.Indonesian;
		 	    mdlMessage.English = mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
				LogAdapter.InsertLog(mdlLog);
				return mdlGetLoginCardResult;
			}

            outputSchemaMap = new LinkedTreeMap<String, JsonArray>();
            mObj = new JsonObject();

            outputSchemaMap = (LinkedTreeMap<String, JsonArray>) mdlAPIResult.OutputSchema;
            mObj = gson.toJsonTree(outputSchemaMap).getAsJsonObject();
			collectionType = new TypeToken<List<model.mdlAccount>>(){}.getType();
			listAccount = gson.fromJson( mObj.get("AccountList").toString() , collectionType);

				//CHECK IF RETURN CAN BE NOT NULL!!!
	 	    if (listAccount.size() == 0){
	 	    	mdlErrorSchema.ErrorCode = "06";
	 	    	mdlMessage.Indonesian = "Tidak ada akun";
	 	    	mdlMessage.English = mdlLog.ErrorMessage = "No account existed";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
				LogAdapter.InsertLog(mdlLog);
				return mdlGetLoginCardResult;
	 	    }

	 	    //if success get list of account, only show account that is not a joint account
 	    	for (model.mdlAccount account : listAccount){
 	    		Integer customerCounter = account.CustomerDetails.size();
 	    		if (customerCounter == 1){
 	    			listAccountFiltered.add(account);
 	    		}else{
 	    			boolean isJointAccount = false;
 	    			List<model.mdlCustomer> listCustomer = account.CustomerDetails;
 	    			for (model.mdlCustomer customer : listCustomer ){
 	    				if ( customer.OwnerCode.equalsIgnoreCase("902")
    						&& (customer.RelationType.equalsIgnoreCase("1") || customer.RelationType.equalsIgnoreCase("2") || customer.RelationType.equalsIgnoreCase("3")) ){
 	    					isJointAccount = true;
 	 	    			}
 	    			}
 	    			if (param.JointAccount.equalsIgnoreCase("true") && isJointAccount) {
 	    				listAccountFiltered.add(account);
 	    			}
 	    		}
 	    	}

 	    	if (listAccountFiltered.size() == 0){
 	    		mdlErrorSchema.ErrorCode = "07";
 	    		mdlMessage.Indonesian = "Tidak ada akun";
 	    		mdlMessage.English = mdlLog.ErrorMessage = "No account existed";
 	    		mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
 	    	}else{
 	    		mdlErrorSchema.ErrorCode = "00";
 	    		mdlMessage.Indonesian = "Berhasil";
 	    		mdlMessage.English = mdlLog.LogStatus = "Success";
 	    		mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
 	    		mdlGetLoginCardResult.OutputSchema = listAccountFiltered;
 	    	}
		}catch(Exception ex){
			mdlErrorSchema.ErrorCode = "08";
			mdlMessage.Indonesian = "Gagal memanggil service";
			mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
			mdlErrorSchema.ErrorMessage = mdlMessage;
			mdlGetLoginCardResult.ErrorSchema = mdlErrorSchema;
			mdlLog.ErrorMessage = ex.toString();
		}

		LogAdapter.InsertLog(mdlLog);
		return mdlGetLoginCardResult;
	}

    @RequestMapping(value = "/loginDeviceManagement",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlAPIResult LoginDeviceManagement(@RequestBody model.mdlLoginDeviceManagement param)
	{
		model.mdlAPIResult mdlLoginDeviceManagementResult = new model.mdlAPIResult();
		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
		model.mdlMessage mdlMessage = new model.mdlMessage();
		model.mdlResult mdlResult = new model.mdlResult();

		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.WSID = "";
		mdlLog.SerialNumber = param.SerialNumber;
		mdlLog.ApiFunction = "loginDeviceManagement";
		mdlLog.SystemFunction = "LoginDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.LogStatus = "Failed";

		//model.mdlLoginDeviceManagement loginDevice =  new model.mdlLoginDeviceManagement();
		String UserID = (param.UserID == null) ? "" : param.UserID.trim();
		String Password = (param.Password == null) ? "" : param.Password.trim();
		Boolean checkLogin = false;

		if (UserID.equals("") || Password.equals("")){
			mdlErrorSchema.ErrorCode = "01";
			mdlMessage.Indonesian = "User ID atau Password invalid";
			mdlMessage.English = mdlLog.ErrorMessage = "Invalid User ID or Password";
			mdlErrorSchema.ErrorMessage = mdlMessage;
			mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
			LogAdapter.InsertLog(mdlLog);
			return mdlLoginDeviceManagementResult;
		}else{
			try {
				checkLogin = DeviceManagementAdapter.CheckLogin(param);
				if (checkLogin){
					mdlErrorSchema.ErrorCode = "00";
					mdlMessage.Indonesian = "Berhasil";
					mdlMessage.English = mdlLog.LogStatus = "Success";
					mdlErrorSchema.ErrorMessage = mdlMessage;
					mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
					mdlResult.Result = checkLogin.toString();
					mdlLoginDeviceManagementResult.OutputSchema = mdlResult;
				}else{
					mdlErrorSchema.ErrorCode = "02";
					mdlMessage.Indonesian = "User ID atau Password salah";
					mdlMessage.English = mdlLog.ErrorMessage = "Wrong User ID or Password";
					mdlErrorSchema.ErrorMessage = mdlMessage;
					mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
				}
			  } catch (Exception ex){
				  mdlErrorSchema.ErrorCode = "03";
				  mdlMessage.Indonesian = "Gagal";
				  mdlMessage.English = mdlLog.ErrorMessage = "Failed";
				  mdlErrorSchema.ErrorMessage = mdlMessage;
				  mdlLoginDeviceManagementResult.ErrorSchema = mdlErrorSchema;
			  }
		}
		LogAdapter.InsertLog(mdlLog);
		return mdlLoginDeviceManagementResult;
	}

    @RequestMapping(value = "/getDeviceManagement",method = RequestMethod.GET)
   	public @ResponseBody model.mdlAPIResult GetDeviceManagement(@RequestParam(value="serial", defaultValue="") String SerialNumber)
   	{
   		model.mdlAPIResult mdlGetDeviceManagementResult = new model.mdlAPIResult();
   		model.mdlDeviceManagement DeviceManagement =  new model.mdlDeviceManagement();
   		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
   		model.mdlMessage mdlMessage = new model.mdlMessage();

   		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.WSID = SerialNumber;
		mdlLog.SerialNumber = "";
		mdlLog.ApiFunction = "getDeviceManagement";
		mdlLog.SystemFunction = "GetDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.LogStatus = "Failed";

   		try {
   			DeviceManagement = DeviceManagementAdapter.GetDeviceManagement(SerialNumber);
   			if (DeviceManagement.SerialNumber == null){

   				mdlErrorSchema.ErrorCode = "01";
   				mdlMessage.Indonesian = "Tidak Ada Device";
   				mdlMessage.English = mdlLog.ErrorMessage = "No Device";
   				mdlErrorSchema.ErrorMessage = mdlMessage;
   				mdlGetDeviceManagementResult.ErrorSchema = mdlErrorSchema;
   			}
   			else{
   				mdlErrorSchema.ErrorCode = "00";
   				mdlMessage.Indonesian = "Berhasil";
   				mdlMessage.English = mdlLog.LogStatus = "Success";
   				mdlErrorSchema.ErrorMessage = mdlMessage;
   				mdlGetDeviceManagementResult.ErrorSchema = mdlErrorSchema;
   				mdlGetDeviceManagementResult.OutputSchema = DeviceManagement;
   			}
   		}catch(Exception ex){
   			mdlErrorSchema.ErrorCode = "02";
   			mdlMessage.Indonesian = "Gagal memanggil service";
   			mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
   			mdlErrorSchema.ErrorMessage = mdlMessage;
   			mdlGetDeviceManagementResult.ErrorSchema = mdlErrorSchema;

   		}
   		LogAdapter.InsertLog(mdlLog);
   		return mdlGetDeviceManagementResult;
   	}

    @RequestMapping(value = "/getBranchList",method = RequestMethod.GET)
   	public @ResponseBody model.mdlAPIResult GetBranchList(@RequestParam(value="serial", defaultValue="") String SerialNumber,
   														  @RequestParam(value="wsid", defaultValue="") String WSID )
	{
		model.mdlAPIResult mdlGetBranchListResult = new model.mdlAPIResult();
		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
		model.mdlMessage mdlMessage = new model.mdlMessage();

		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.WSID = WSID;
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.ApiFunction = "getBranchList";
		mdlLog.SystemFunction = "GetBranchList";
		mdlLog.LogSource = "Webservice";
		mdlLog.LogStatus = "Failed";

		Integer checkWSID = 1;
		try {
			checkWSID = DeviceManagementAdapter.CheckWSID(SerialNumber, WSID);
			if (checkWSID == 1){
				mdlErrorSchema.ErrorCode = "01";
				mdlMessage.Indonesian = "WSID Sudah Ada";
				mdlMessage.English = mdlLog.ErrorMessage = "WSID Already Exists";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
				LogAdapter.InsertLog(mdlLog);
				return mdlGetBranchListResult;
			}else if(checkWSID == 2){
				mdlErrorSchema.ErrorCode = "02";
				mdlMessage.Indonesian = "Pengecekan WSID gagal";
				mdlMessage.English = mdlLog.ErrorMessage = "WSID check failed";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
				LogAdapter.InsertLog(mdlLog);
				return mdlGetBranchListResult;
			}

			String branchCode = WSID.substring(0, 4);

			List<model.mdlBranch> branchList = DeviceManagementAdapter.GetBranchList(branchCode, SerialNumber, WSID);

			if (branchList.size() == 0){
				mdlErrorSchema.ErrorCode = "03";
				mdlMessage.Indonesian = "Cabang tidak ditemukan";
				mdlMessage.English = mdlLog.ErrorMessage = "Branch not found";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
				LogAdapter.InsertLog(mdlLog);
				return mdlGetBranchListResult;
			}else{
				mdlErrorSchema.ErrorCode = "00";
				mdlMessage.Indonesian = "Berhasil";
				mdlMessage.English = mdlLog.LogStatus = "Success";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
				mdlGetBranchListResult.OutputSchema = branchList;
			}
		}catch(Exception ex){
			mdlErrorSchema.ErrorCode = "04";
			mdlMessage.Indonesian = "Gagal memanggil service";
			mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
			mdlErrorSchema.ErrorMessage = mdlMessage;
			mdlGetBranchListResult.ErrorSchema = mdlErrorSchema;
		}
		LogAdapter.InsertLog(mdlLog);
		return mdlGetBranchListResult;
	}

    @RequestMapping(value = "/updateDeviceManagement",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlAPIResult UpdateDeviceManagement(@RequestBody model.mdlDeviceManagement param)
	{
		model.mdlAPIResult mdlUpdateDeviceManagementResult = new model.mdlAPIResult();
		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
		model.mdlMessage mdlMessage = new model.mdlMessage();
		model.mdlResult mdlResult = new model.mdlResult();
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.WSID = param.WSID;
		mdlLog.SerialNumber = param.SerialNumber;
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "UpdateDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.LogStatus = "Failed";

		Boolean checkDeviceManagement, checkBranchCode, isSuccess = false;

		try {
			checkBranchCode = DeviceManagementAdapter.CheckBranchCode(param.BranchCode, param.BranchTypeID, param.BranchInitial, param.SerialNumber, param.WSID);
			if (!checkBranchCode){
				mdlErrorSchema.ErrorCode = "01";
				mdlMessage.Indonesian = "Kode Cabang Salah Input";
				mdlMessage.English = mdlLog.ErrorMessage = "Wrong Input Branch Code";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
				mdlUpdateDeviceManagementResult.OutputSchema = checkBranchCode;
				LogAdapter.InsertLog(mdlLog);
				return mdlUpdateDeviceManagementResult;
			}

			checkDeviceManagement = DeviceManagementAdapter.CheckDeviceManagement(param.SerialNumber, param.WSID);
			//isSuccess = (checkDeviceManagement) ? DeviceManagementAdapter.UpdateDeviceManagement(param) : DeviceManagementAdapter.InsertDeviceManagement(param);
			if(checkDeviceManagement){
				//if serial number exists, then update existing data
				isSuccess = DeviceManagementAdapter.UpdateDeviceManagement(param);
			}else{
				//if serial number not exists, insert new data
				isSuccess = DeviceManagementAdapter.InsertDeviceManagement(param);
			}

			if (isSuccess){
				mdlErrorSchema.ErrorCode = "00";
				mdlMessage.Indonesian = "Data Berhasil Disimpan";
				mdlMessage.English = "Data Successfully Saved";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
				mdlUpdateDeviceManagementResult.OutputSchema = isSuccess;
				mdlResult.Result = isSuccess.toString();
				mdlUpdateDeviceManagementResult.OutputSchema = mdlResult;
				mdlLog.LogStatus = "Success";
			}
			else{
				mdlErrorSchema.ErrorCode = "02";
				mdlMessage.Indonesian = "Data Gagal Disimpan";
				mdlMessage.English = mdlLog.ErrorMessage = "Save Data Failed";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
			}
		}catch(Exception ex){
			mdlErrorSchema.ErrorCode = "03";
			mdlMessage.Indonesian = "Gagal memanggil service";
			mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
			mdlErrorSchema.ErrorMessage = mdlMessage;
			mdlUpdateDeviceManagementResult.ErrorSchema = mdlErrorSchema;
		}
		LogAdapter.InsertLog(mdlLog);
		return mdlUpdateDeviceManagementResult;
	}

    @RequestMapping(value = "/transactionLog",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlAPIResult TransactionLog(@RequestBody model.mdlTransactionLog param)
	{
		model.mdlAPIResult mdlTransactionLogResult = new model.mdlAPIResult();
		model.mdlErrorSchema mdlErrorSchema = new model.mdlErrorSchema();
		model.mdlMessage mdlMessage = new model.mdlMessage();
		model.mdlResult mdlResult = new model.mdlResult();
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.WSID = param.WSID;
		mdlLog.SerialNumber = param.SerialNumber;
		mdlLog.ApiFunction = "transactionLog";
		mdlLog.SystemFunction = "TransactionLog";
		mdlLog.LogSource = "Webservice";
		mdlLog.LogStatus = "Failed";

		Boolean isSuccess = false;

		try {
			isSuccess = LogAdapter.InsertTransactionLog(param);

			if (isSuccess){
				mdlErrorSchema.ErrorCode = "00";
				mdlMessage.Indonesian = "Log berhasil disimpan";
				mdlMessage.English = "Log successfully saved";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlTransactionLogResult.ErrorSchema = mdlErrorSchema;
				mdlResult.Result = isSuccess.toString();
				mdlTransactionLogResult.OutputSchema = mdlResult;
				mdlLog.LogStatus = "Success";
			}
			else{
				mdlErrorSchema.ErrorCode = "01";
				mdlMessage.Indonesian = "Log gagal disimpan";
				mdlMessage.English = mdlLog.ErrorMessage = "Save log failed";
				mdlErrorSchema.ErrorMessage = mdlMessage;
				mdlTransactionLogResult.ErrorSchema = mdlErrorSchema;
			}
		}catch(Exception ex){
			mdlErrorSchema.ErrorCode = "02";
			mdlMessage.Indonesian = "Gagal memanggil service";
			mdlMessage.English = mdlLog.ErrorMessage = "Service call failed";
			mdlErrorSchema.ErrorMessage = mdlMessage;
			mdlTransactionLogResult.ErrorSchema = mdlErrorSchema;
			//LogAdapter.InsertLog(ex.toString(), "GetDepartmentAPI", Globals.gCommand , lUser);
		}
		LogAdapter.InsertLog(mdlLog);
		return mdlTransactionLogResult;
	}
}
