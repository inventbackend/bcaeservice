package adapter;

import adapter.Base64Adapter;

public class ClientIdAdapter {
	public static String getAuthorizationKey(){
		model.mdlClientId mdlClientId = new model.mdlClientId();
		String stringToSign = mdlClientId.ClientID + ":" + mdlClientId.ClientSecret;
		String authorizationKey  = Base64Adapter.EncriptBase64(stringToSign); //generate apiKey
		return authorizationKey;
	}
}
