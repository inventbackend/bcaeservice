package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;

import adapter.ConvertDateTimeHelper;
import model.Globals;

/** Documentation
 *
 */
public class LogAdapter {

	public static void InsertLog(model.mdlLog logModel)
	{
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		try
		{
			String newLogID = CreateLogID();
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			String sql = "INSERT INTO APILog (LogID, LogDate, LogSource, SerialNumber, WSID, APIFunction, SystemFunction, LogStatus, ErrorMessage)"
						+ "VALUES (?,TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'),?,?,?,?,?,?,?)";

			pstm = connection.prepareStatement(sql);

			//insert sql parameter
			String dateNow = LocalDateTime.now().toString().replace("T", " ");
			pstm.setString(1, newLogID);
			pstm.setString(2, dateNow);
			pstm.setString(3, logModel.LogSource);
			pstm.setString(4, logModel.SerialNumber);
			pstm.setString(5, logModel.WSID);
			pstm.setString(6, logModel.ApiFunction);
			pstm.setString(7, logModel.SystemFunction);
			pstm.setString(8, logModel.LogStatus);
			pstm.setString(9, logModel.ErrorMessage);

			//execute query
			jrs = pstm.executeQuery();
		}
		catch(Exception ex) {
			System.out.print(ex.toString());
		}
		finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				Globals.gReturn_Status = "Insert log failed";
			}
		}
		return;
	}

	public static Boolean InsertTransactionLog(model.mdlTransactionLog mdlTransactionLog)
	{
		boolean isSuccess = false;
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		try
		{
			String newTransactionID = CreateTransactionID();
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			String sql = "INSERT INTO TransactionLog (TransactionID, SerialNumber, WSID, StartTime, EndTime, CustomerNumber, AccountNumber, LoginType, LoginNumber, BranchCode, TransactionType, LogoutReason, CreatedDate)"
						+ "VALUES (?,?,?,TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'),TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'),?,?,?,?,?,?,?,TO_TIMESTAMP(?,'YYYY-MM-DD HH24:MI:SS.FF'))";

			pstm = connection.prepareStatement(sql);

			//insert sql parameter
			pstm.setString(1, newTransactionID);
			pstm.setString(2, mdlTransactionLog.SerialNumber);
			pstm.setString(3, mdlTransactionLog.WSID);
			pstm.setString(4, mdlTransactionLog.StartTime);
			pstm.setString(5, mdlTransactionLog.EndTime);
			pstm.setString(6, mdlTransactionLog.CustomerNumber);
			pstm.setString(7, mdlTransactionLog.AccountNumber);
			pstm.setString(8, mdlTransactionLog.LoginType);
			pstm.setString(9, mdlTransactionLog.LoginNumber);
			pstm.setString(10, mdlTransactionLog.BranchCode);
			pstm.setString(11, mdlTransactionLog.TransactionType);
			pstm.setString(12, mdlTransactionLog.LogoutReason);
			pstm.setString(13, LocalDateTime.now().toString().replace("T", " "));

			//execute query
			jrs = pstm.executeQuery();
			isSuccess = true;
		}
		catch(Exception ex) {
			System.out.print(ex.toString());
		}
		finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {
				Globals.gReturn_Status = "Insert log failed";
			}
		}
		return isSuccess;
	}

	public static String CreateTransactionID(){
		String lastTransactionID = getLastTransactionID();
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "00000001";

		if ((lastTransactionID != null) && (!lastTransactionID.equals("")) ){
			String[] partsTransactionID = lastTransactionID.split("-");
			String partDate = partsTransactionID[1];
			String partNumber = partsTransactionID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%08d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("TRA-").append(datetime).append("-").append(stringInc);
		String TransactionID = sb.toString();
		return TransactionID;
	}

	public static String getLastTransactionID(){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String TransactionID = "";
		try{
			connection = database.RowSetAdapter.getConnectionWL();
			pstm = connection.prepareStatement("SELECT NVL(MAX(TransactionID),'') as TransactionID FROM TransactionLog");
			Globals.gCommand = pstm.toString();

			jrs = pstm.executeQuery();

			while(jrs.next())
			{
				TransactionID = jrs.getString("TransactionID");
			}
		}
		catch (Exception ex){
			System.out.print(ex.toString());
		}
		finally{
			//close the opened connection
			try{
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception ex){

			}
		}
		return TransactionID;
	}

	public static String CreateLogID(){
		String lastLogID = getLastLogID();
		String dateNow = LocalDateTime.now().toString().substring(0, 10).toString();
		String datetime = ConvertDateTimeHelper.formatDate(dateNow, "yyyy-MM-dd", "yyyyMMdd");
		String stringInc = "00000001";

		if ((lastLogID != null) && (!lastLogID.equals("")) ){
			String[] partsLogID = lastLogID.split("-");
			String partDate = partsLogID[1];
			String partNumber = partsLogID[2];

			//check if the date is same, if same, add 1 to the number
			if (partDate.equals(datetime)){
				int inc = Integer.parseInt(partNumber) + 1;
				stringInc = String.format("%08d", inc);
			}
		}
		StringBuilder sb = new StringBuilder();
		sb.append("API-").append(datetime).append("-").append(stringInc);
		String LogID = sb.toString();
		return LogID;
	}

	public static String getLastLogID(){
		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		String LogID = "";
		try{
			connection = database.RowSetAdapter.getConnectionWL();
			pstm = connection.prepareStatement("SELECT NVL(MAX(LogID),'') as LogID FROM APILog");
			Globals.gCommand = pstm.toString();

			jrs = pstm.executeQuery();

			while(jrs.next())
			{
				LogID = jrs.getString("LogID");
			}
		}
		catch (Exception ex){
			System.out.print(ex.toString());
		}
		finally{
			//close the opened connection
			try{
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception ex){

			}
		}
		return LogID;
	}

}
