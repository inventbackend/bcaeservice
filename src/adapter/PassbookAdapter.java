package adapter;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import model.Globals;

public class PassbookAdapter {
	public static model.mdlAPIResult getPassbookHeader(model.mdlChangePassbook changePassBook){
		model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
		model.mdlClientId mdlClientId = new model.mdlClientId();
		String jsonIn, jsonOut = "";
		String urlAPI = "/passbook/header";
		String urlFinal = Globals.urlBase + urlAPI;

		try {
			Client client = Client.create();
			WebResource webResource = client.resource(urlFinal);
			Gson gson = new Gson();
			jsonIn = gson.toJson(changePassBook);
			jsonIn.replaceAll("\\s+", ""); //canonicalize JSON (remove all whitespace like \r, \n, \t and space)

			ClientResponse response  = webResource.type("application/json")
													.header("ClientID", mdlClientId.ClientID)
													.get(ClientResponse.class);
			jsonOut = response.getEntity(String.class);

			mdlAPIResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
		  } catch (Exception ex) {
			//LogAdapter.InsertLogExc(ex.toString(), "GetDepartmentAPI", Globals.gCommand , lUser);
			//Globals.gReturn_Status = "Error GetToken";
			  mdlAPIResult = null;
		  }

		return mdlAPIResult;
	}
}
