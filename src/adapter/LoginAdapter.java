package adapter;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import adapter.TokenAdapter;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlAccount;
import model.mdlUser;
import model.mdlCardDetails;
import model.mdlHeader;

import adapter.HeaderAdapter;

public class LoginAdapter {
	public static model.mdlAPIResult verifyPIN(model.mdlLoginCard mdlLoginCard){
		model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
		model.mdlClientId mdlClientId = new model.mdlClientId();
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "loginByCard";
		mdlLog.SystemFunction = "verifyPIN";
		mdlLog.LogSource = "Middleware";
		mdlLog.SerialNumber = mdlLoginCard.SerialNumber;
		mdlLog.WSID = mdlLoginCard.WSID;
		mdlLog.LogStatus = "Success";
		mdlLog.ErrorMessage = "";

		String jsonIn, jsonOut = "";
		String cardNumber = mdlLoginCard.CardNumber;
		Gson gson = new Gson();

		String urlAPI = "/debit-cards/pin-verification/card-numbers/"+ cardNumber;
		String urlFinal = Globals.urlBase + urlAPI;
		try{
			Client client = Client.create();
			Globals.gCommand = urlAPI;

			WebResource webResource = client.resource(urlFinal);
			jsonIn = gson.toJson(mdlLoginCard);
			jsonIn.replaceAll("\\s+", ""); //canonicalize JSON (remove all whitespace like \r, \n, \t and space)

			ClientResponse response = webResource.type("application/json")
												 .header("ClientID", mdlClientId.ClientID)
												 .post(ClientResponse.class,jsonIn);
			jsonOut = response.getEntity(String.class);

			//JSON String for test
			//jsonOut = "{\"ErrorSchema\":{\"ErrorCode\": \"ESB-00-000\",\"ErrorMessage\":{\"Indonesian\": \"Berhasil\",\"English\": \"Success\"}},\"OutputSchema\":{\"CardDetails\":{\"CardNumber\":\"123456789\",\"LanguageID\":\"1\",\"CurrentDate\":\"20042018\",\"OpenDate\":\"KCU Thamrin\",\"ProductionDate\":\"T001\",\"CustomerName\":\"111111=9999000000\",\"CustomerAddress\":[\"jl. bojong\",\"jl. thamrin\",\"jl. otong\"],\"BranchCode\":\"02\",\"ProductionCardFlag\":\"02\",\"ChargeFlag\":\"02\",\"Outstanding\":\"02\",\"SendFlag\":\"02\",\"CardCompanyBrand\":\"02\"},\"AccountDetails\":[{\"AccountNumber\":\"111222333\",\"AccountType\":\"7\",\"AccountOwnership\":\"P\"},{\"AccountNumber\":\"444555666\",\"AccountType\":\"8\",\"AccountOwnership\":\"S\"}],\"ResultB24\":\"02\"}}";

			mdlAPIResult = gson.fromJson(jsonOut, model.mdlAPIResult.class);
			if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")){
				mdlLog.LogStatus = "Failed";
				mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
				LogAdapter.InsertLog(mdlLog);
				return mdlAPIResult;
			}
		  } catch (Exception ex) {
			  mdlAPIResult = null;
			  mdlLog.LogStatus = "Failed";
			  mdlLog.ErrorMessage = ex.toString();
			  LogAdapter.InsertLog(mdlLog);
			  return mdlAPIResult;
		  }
		LogAdapter.InsertLog(mdlLog);
		return mdlAPIResult;
	}

	public static model.mdlAPIResult getAccountDetailsByCard(String accountNumberList, String SerialNumber, String WSID){
		model.mdlAPIResult mdlAPIResult = new model.mdlAPIResult();
		model.mdlClientId mdlClientId = new model.mdlClientId();
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "loginByCard";
		mdlLog.SystemFunction = "getAccountDetailsByCard";
		mdlLog.LogSource = "Middleware";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Success";
		mdlLog.ErrorMessage = "";
		String jsonOut = "";

		String urlAPI = "/deposit-accounts/account-number-lists/" + accountNumberList;
		String urlFinal = Globals.urlBase + urlAPI;

		try {
			Client client = Client.create();
			WebResource webResource = client.resource(urlFinal);

			ClientResponse response  = webResource.type("application/json")
													.header("ClientID", mdlClientId.ClientID)
													.get(ClientResponse.class);

			jsonOut = response.getEntity(String.class);

			//JSON FOR TEST.. COMMENT WHEN TESTING TO REAL URL
			//jsonOut = "{\"ErrorSchema\":{\"ErrorCode\":\"ESB-00-000\",\"ErrorMessage\":{\"Indonesian\":\"Berhasil\",\"English\":\"Success\"}},\"OutputSchema\":{\"AccountList\":[{\"AccountNumber\":\"04625450321\",\"AccountName\":\"JORDANRANDALLSUDRADJAT\",\"AccountType\":\"110\",\"AccountTypeDescription\":\"Tahapan\",\"CustomerDetails\":[{\"CustomerNumber\":\"00000000898\",\"CustomerName\":\"JORDANRANDALLSUDRADJAT\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"},{\"CustomerNumber\":\"00000000898\",\"CustomerName\":\"JORDANRANDALLSUDRADJAT\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]},{\"AccountNumber\":\"00210001395\",\"AccountName\":\"\",\"AccountType\":\"\",\"AccountTypeDescription\":null,\"CustomerDetails\":[{\"CustomerNumber\":\"00000006508\",\"CustomerName\":\"JAYAKARINAA\",\"RelationType\":\"1\",\"OwnerCode\":\"901\"},{\"CustomerNumber\":\"00000006508\",\"CustomerName\":\"JAYAKARINAA\",\"RelationType\":\"1\",\"OwnerCode\":\"901\"}]},{\"AccountNumber\":\"04621215851\",\"AccountName\":\"SUGINANAFRANZ\",\"AccountType\":\"110\",\"AccountTypeDescription\":\"Tahapan\",\"CustomerDetails\":[{\"CustomerNumber\":\"00000000878\",\"CustomerName\":\"SUGINANAFRANZ\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]},{\"AccountNumber\":\"04010000021\",\"AccountName\":\"OMARPUTIHRAI\",\"AccountType\":\"110\",\"AccountTypeDescription\":\"Tahapan\",\"CustomerDetails\":[{\"CustomerNumber\":\"00000000290\",\"CustomerName\":\"RICHARDMCAULIFFE\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"}]},{\"AccountNumber\":\"00012011111\",\"AccountName\":\"\",\"AccountType\":\"\",\"AccountTypeDescription\":null,\"CustomerDetails\":[{\"CustomerNumber\":\"00000001500\",\"CustomerName\":\"PAULINE\",\"RelationType\":\"1\",\"OwnerCode\":\"902\"},{\"CustomerNumber\":\"00000003252\",\"CustomerName\":\"NASABAHTESTNPWP\",\"RelationType\":\"4\",\"OwnerCode\":\"902\"},{\"CustomerNumber\":\"00000003257\",\"CustomerName\":\"PHILIP\",\"RelationType\":\"4\",\"OwnerCode\":\"902\"},{\"CustomerNumber\":\"00000003270\",\"CustomerName\":\"NAMA\",\"RelationType\":\"1\",\"OwnerCode\":\"902\"},{\"CustomerNumber\":\"00000000902\",\"CustomerName\":\"SUGINANAFRANZ\",\"RelationType\":\"1\",\"OwnerCode\":\"902\"},{\"CustomerNumber\":\"00000000828\",\"CustomerName\":\"SUGINANAFRANZ\",\"RelationType\":\"1\",\"OwnerCode\":\"902\"},{\"CustomerNumber\":\"00000003253\",\"CustomerName\":\"FERDI\",\"RelationType\":\"1\",\"OwnerCode\":\"902\"},{\"CustomerNumber\":\"00000003255\",\"CustomerName\":\"AGUSTINA\",\"RelationType\":\"0\",\"OwnerCode\":\"901\"},{\"CustomerNumber\":\"00000003256\",\"CustomerName\":\"YOSEPH\",\"RelationType\":\"1\",\"OwnerCode\":\"902\"}]}]}}";

			Gson gson = new Gson();
			mdlAPIResult = gson.fromJson(jsonOut, mdlAPIResult.getClass());
			if (!mdlAPIResult.ErrorSchema.ErrorCode.equalsIgnoreCase("ESB-00-000")){
				mdlLog.LogStatus = "Failed";
				mdlLog.ErrorMessage = mdlAPIResult.ErrorSchema.ErrorMessage.English;
				LogAdapter.InsertLog(mdlLog);
				return mdlAPIResult;
			}
		  } catch (Exception ex) {
			  mdlAPIResult = null;
			  mdlLog.LogStatus = "Failed";
			  mdlLog.ErrorMessage = ex.toString();
			  LogAdapter.InsertLog(mdlLog);
			  return mdlAPIResult;
		  }
		LogAdapter.InsertLog(mdlLog);
		return mdlAPIResult;
	}
}
