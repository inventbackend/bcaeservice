package adapter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class HeaderAdapter {
	public static model.mdlHeader GetHeaders(String httpMethod, String url, String token, String body){
		model.mdlHeader mdlHeader = new model.mdlHeader();
		String timestamp = ZonedDateTime.now().format( DateTimeFormatter.ISO_DATE_TIME ).substring(0, 29);
		String apiKey = "f4a423d1-df80-4d5d-b618-15ec79810a8f"; //key from BCA
		String apiSecret = "c5e78dfa-bc8b-4510-8598-f0ed0d8ad236";
		//encrypt body content with SHA256 and change it into lowercase
		String hashedBody = sha256(body).toLowerCase();
		String stringToSign = httpMethod + ":" + url + ":" + token + ":" + hashedBody + ":" + timestamp;
		String signature = generateHmacSHA256Signature(apiSecret, stringToSign);
		mdlHeader.Timestamp = timestamp;
		mdlHeader.Key = apiKey;
		mdlHeader.Signature = signature;

		return mdlHeader;
	}

	public static String sha256(String base) {
	    try{
	        MessageDigest digest = MessageDigest.getInstance("SHA-256");
	        byte[] hash = digest.digest(base.getBytes("UTF-8"));
	        StringBuffer hexString = new StringBuffer();
	        for (int i = 0; i < hash.length; i++) {
	            String hex = Integer.toHexString(0xff & hash[i]);
	            if(hex.length() == 1) hexString.append('0');
	            hexString.append(hex);
	        }
	        return hexString.toString();
	    } catch(Exception ex){
	       throw new RuntimeException(ex);
	    }
	}

	public static String generateHmacSHA256Signature(String key, String data)
	//throws GeneralSecurityException, IOException
	{
        String algorithm = "HmacSHA256";  // OPTIONS= HmacSHA512, HmacSHA256, HmacSHA1, HmacMD5
        String hash = "";
        try {
            // 1. Get an algorithm instance.
            Mac sha256_hmac = Mac.getInstance(algorithm);

            // 2. Create secret key.
            SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);

            // 3. Assign secret key algorithm.
            sha256_hmac.init(secret_key);

            // Encode the string into bytes using utf-8 and digest it
            byte[] digest = sha256_hmac.doFinal(data.getBytes("UTF-8"));

            // Convert digest into a hex string
            hash = byteArrayToHex(digest);

        } catch (Exception e) {
        	model.mdlLog mdlLog = new model.mdlLog();
        	mdlLog.ApiFunction = "generateHmacSHA256Signature";
        	mdlLog.LogSource = "Webservice";
        	mdlLog.LogStatus = "Failed";
        	mdlLog.ErrorMessage = e.toString();
        	LogAdapter.InsertLog(mdlLog);
        }/* catch (NoSuchAlgorithmException e) {
            e.printStackTrace(); //TODO: log into database if there is error
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace(); //TODO: log into database if there is error
        } catch (InvalidKeyException e) {
            e.printStackTrace(); //TODO: log into database if there is error
        }*/
        return hash;
    }

	public static String byteArrayToHex(byte[] a) {
		StringBuilder sb = new StringBuilder(a.length * 2);
		for(byte b: a)
			sb.append(String.format("%02x", b));
		return sb.toString();
	}
}
