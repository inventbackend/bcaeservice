package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DeviceManagementAdapter {

	public static boolean CheckLogin(model.mdlLoginDeviceManagement mdlLoginDeviceManagement){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "loginDeviceManagement";
		mdlLog.SystemFunction = "CheckLogin";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = mdlLoginDeviceManagement.SerialNumber;
		mdlLog.WSID = "";
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;

		boolean userExists = false;
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			//call store procedure
			String sql = "SELECT UserID, Password FROM ms_user WHERE UserID = ? AND Password = ? FETCH FIRST 1 ROWS ONLY";

			pstm = connection.prepareStatement(sql);
			pstm.setString(1, mdlLoginDeviceManagement.UserID);
			pstm.setString(2, mdlLoginDeviceManagement.Password);
			//execute query
			jrs = pstm.executeQuery();

			while(jrs.next()){
				userExists = true;
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception ex) {

			}
		}
		return userExists;
	}

	public static model.mdlDeviceManagement GetDeviceManagement(String SerialNumber){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "getDeviceManagement";
		mdlLog.SystemFunction = "GetDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = "";
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		model.mdlDeviceManagement DeviceManagement = new model.mdlDeviceManagement();

		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			//set sql string
			String sql = "SELECT DEVICE.SERIALNUMBER, DEVICE.WSID, DEVICE.BRANCHCODE, BRANCH.BRANCHNAME, DEVICE.BRANCHTYPEID, "
						+ "DEVICE.BRANCHINITIAL, BRANCHTYPE.BRANCHTYPENAME, DEVICE.UPDATEBY, DEVICE.UPDATEDATE FROM MS_DEVICE DEVICE "
						+ "LEFT JOIN MS_BRANCH BRANCH ON DEVICE.BRANCHCODE = BRANCH.BRANCHCODE AND DEVICE.BRANCHTYPEID = BRANCH.BRANCHTYPEID "
						+ "AND DEVICE.BRANCHINITIAL = BRANCH.BRANCHINITIAL "
						+ "LEFT JOIN MS_BRANCHTYPE BRANCHTYPE ON DEVICE.BRANCHTYPEID = BRANCHTYPE.BRANCHTYPEID "
						+ "WHERE DEVICE.SERIALNUMBER = ?";
			pstm = connection.prepareStatement(sql);

			//insert sql parameter
			pstm.setString(1, SerialNumber);

			//execute query
			jrs = pstm.executeQuery();

			while(jrs.next()){
				DeviceManagement.SerialNumber = jrs.getString("SERIALNUMBER");
				DeviceManagement.WSID = jrs.getString("WSID");
				DeviceManagement.BranchCode = jrs.getString("BRANCHCODE");
				DeviceManagement.BranchInitial = jrs.getString("BRANCHINITIAL");
				DeviceManagement.BranchTypeID = jrs.getString("BRANCHTYPEID");
				DeviceManagement.BranchName = jrs.getString("BRANCHNAME");
				DeviceManagement.BranchTypeName = jrs.getString("BRANCHTYPENAME");
				DeviceManagement.UpdateBy = jrs.getString("UPDATEBY");
				DeviceManagement.UpdateDate = jrs.getString("UPDATEDATE");

			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				if (pstm != null) pstm.close();
				if (connection != null) connection.close();
				if (jrs != null) jrs.close();
			}
			catch(Exception e) {

			}
		}
		return DeviceManagement;
	}

	public static boolean CheckDeviceManagement(String SerialNumber, String WSID){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "CheckDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		boolean isExists = false;
		//check if device is exists or not
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();

			//call store procedure
			String sql = "SELECT SerialNumber FROM ms_device WHERE SerialNumber = ? FETCH FIRST 1 ROWS ONLY";
			pstm = connection.prepareStatement(sql);
			//insert sql parameter
			pstm.setString(1, SerialNumber);
			//execute query
			jrs = pstm.executeQuery();
			while(jrs.next()){
				isExists = true;
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {

			}
		}
		return isExists;
	}

	public static Integer CheckWSID(String SerialNumber, String WSID){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "getBranchList";
		mdlLog.SystemFunction = "CheckWSID";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		Integer returnValue = 0;
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			String sql = "SELECT WSID FROM ms_device WHERE WSID = ?";
			pstm = connection.prepareStatement(sql);
			//insert sql parameter
			pstm.setString(1, WSID);
			//execute query
			jrs = pstm.executeQuery();
			while(jrs.next()){
				returnValue = 1;
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			returnValue = 2;

			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {

			}
		}
		return returnValue;
	}

	public static boolean CheckBranchCode(String BranchCode, String BranchTypeID, String BranchInitial, String SerialNumber, String WSID){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "CheckBranchCode";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		boolean isExists = false;
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();

			//call store procedure
			String sql = "SELECT BranchCode FROM ms_branch WHERE BranchCode = ? AND BranchTypeID = ? AND BranchInitial = ?";
			pstm = connection.prepareStatement(sql);
			//insert sql parameter
			pstm.setString(1, BranchCode);
			pstm.setString(2, BranchTypeID);
			pstm.setString(3, BranchInitial);
			//execute query
			jrs = pstm.executeQuery();
			while(jrs.next()){
				isExists = true;
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {

			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {

			}
		}
		return isExists;
	}

	public static List<model.mdlBranch> GetBranchList(String branchCode, String SerialNumber, String WSID){
		List<model.mdlBranch> branchList = new ArrayList<model.mdlBranch>();

		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "getBranchList";
		mdlLog.ApiFunction = "GetBranchList";
		mdlLog.SystemFunction = "CheckBranchCode";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = SerialNumber;
		mdlLog.WSID = WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();

			//call store procedure
			String sql = "SELECT branch.BranchCode, branch.BranchName, branch.BranchTypeID, branchtype.BranchTypeName, branch.BranchInitial FROM ms_branch branch "
						+ "LEFT JOIN ms_branchtype branchtype ON branch.BranchTypeID = branchtype.branchtypeid "
						+ "WHERE BranchCode = ?";
			pstm = connection.prepareStatement(sql);
			//insert sql parameter
			pstm.setString(1, branchCode);
			//execute query
			jrs = pstm.executeQuery();
			while(jrs.next()){
				model.mdlBranch branch = new model.mdlBranch();
				branch.BranchCode = jrs.getString("BranchCode");
				branch.BranchName = jrs.getString("BranchName");
				branch.BranchTypeID = jrs.getString("BranchTypeID");
				branch.BranchTypeName = jrs.getString("BranchTypeName");
				branch.BranchInitial = jrs.getString("BranchInitial");
				branchList.add(branch);
			}
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {

			}
		}
		//do log

		return branchList;
	}

	public static Boolean InsertDeviceManagement(model.mdlDeviceManagement mdlDeviceManagement){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "InsertDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = mdlDeviceManagement.SerialNumber;
		mdlLog.WSID = mdlDeviceManagement.WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		boolean isSuccess = false;
		//check if device is exists or not
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			//call store procedure
			String sql = "INSERT INTO ms_device (SerialNumber,WSID,BranchCode,BranchTypeID,BranchInitial,UpdateDate,UpdateBy) "
						+ "VALUES (?,?,?,?,?,TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS.FF'),?)";
			pstm = connection.prepareStatement(sql);

			//insert sql parameter
			pstm.setString(1, mdlDeviceManagement.SerialNumber);
			pstm.setString(2, mdlDeviceManagement.WSID);
			pstm.setString(3, mdlDeviceManagement.BranchCode);
			pstm.setString(4, mdlDeviceManagement.BranchTypeID);
			pstm.setString(5, mdlDeviceManagement.BranchInitial);
			pstm.setString(6, mdlDeviceManagement.UpdateDate);
			pstm.setString(7, mdlDeviceManagement.UpdateBy);

			//execute query
			jrs = pstm.executeQuery();
			isSuccess = true;
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {

			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {

			}
		}
		return isSuccess;
	}

	public static Boolean UpdateDeviceManagement(model.mdlDeviceManagement mdlDeviceManagement){
		model.mdlLog mdlLog = new model.mdlLog();
		mdlLog.ApiFunction = "updateDeviceManagement";
		mdlLog.SystemFunction = "UpdateDeviceManagement";
		mdlLog.LogSource = "Webservice";
		mdlLog.SerialNumber = mdlDeviceManagement.SerialNumber;
		mdlLog.WSID = mdlDeviceManagement.WSID;
		mdlLog.LogStatus = "Failed";
		mdlLog.ErrorMessage = "";

		Connection connection = null;
		PreparedStatement pstm = null;
		ResultSet jrs = null;
		boolean isSuccess = false;
		//check if device is exists or not
		try{
			//define connection
			connection = database.RowSetAdapter.getConnectionWL();
			//call store procedure
			String sql = "UPDATE ms_device SET WSID = ?, BranchCode = ?, BranchTypeID = ?, UpdateDate = TO_TIMESTAMP(?, 'YYYY-MM-DD HH24:MI:SS.FF'), "
						+ "UpdateBy = ?, BranchInitial = ? WHERE SerialNumber = ?";
			pstm = connection.prepareStatement(sql);

			//insert sql parameter
			pstm.setString(1, mdlDeviceManagement.WSID);
			pstm.setString(2, mdlDeviceManagement.BranchCode);
			pstm.setString(3, mdlDeviceManagement.BranchTypeID);
			pstm.setString(4, mdlDeviceManagement.UpdateDate);
			pstm.setString(5, mdlDeviceManagement.UpdateBy);
			pstm.setString(6, mdlDeviceManagement.BranchInitial);
			pstm.setString(7, mdlDeviceManagement.SerialNumber);

			//execute query
			jrs = pstm.executeQuery();
			isSuccess = true;
			mdlLog.LogStatus = "Success";
		}
		catch(Exception ex) {
			mdlLog.ErrorMessage = ex.toString();
			LogAdapter.InsertLog(mdlLog);
		}
		finally {
			try {
				//close the opened connection
				 if (pstm != null) pstm.close();
				 if (connection != null) connection.close();
				 if (jrs != null) jrs.close();
			}
			catch(Exception e) {

			}
		}
		return isSuccess;
	}
}