package adapter;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;

import com.google.gson.Gson;

import model.Globals;
/** Documentation
 *
 */
public class TokenAdapter {

	public static model.mdlToken GetToken()
	{
		model.mdlToken mdlToken = new model.mdlToken();
		String jsonOut = "";
		try {
			Client client = Client.create();

			String urlAPI = "https://sandbox.bca.co.id/api/oauth/token";
			String authorizationKey = ClientIdAdapter.getAuthorizationKey();

			model.mdlGetToken mdlGetToken = new model.mdlGetToken();
			mdlGetToken.grant_type = "client_credentials";

			WebResource webResource = client.resource(urlAPI);

			//add form variables
            Form form = new Form();
            form.add("grant_type", "client_credentials");

            ClientResponse response  = webResource.type("application/x-www-form-urlencoded")
					  .header("Authorization","Basic "+ authorizationKey)
					  .post(ClientResponse.class,form);

			jsonOut = response.getEntity(String.class);
			Gson gson = new Gson();
			mdlToken = gson.fromJson(jsonOut, model.mdlToken.class);
		}
		catch (Exception ex) {
			//LogAdapter.InsertLogExc(ex.toString(), "GetToken", Globals.gCommand , Globals.user);
			Globals.gReturn_Status = "Error GetToken";
		  }

		return mdlToken;
	}

}
