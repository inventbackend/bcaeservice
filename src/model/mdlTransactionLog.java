package model;

public class mdlTransactionLog {
	public String TransactionID; //varchar2(21) - Primary Key - auto increment ex : TRA-20180508-00000001
	public String SerialNumber; //varchar2(50) can not be null
	public String WSID; //varchar2(50) - can not be null
	public String StartTime; //timestamp
	public String EndTime; //timestamp
	public String CustomerNumber; //varchar2(11)
	public String AccountNumber; //varchar2(11)
	public String LoginType; //varchar2(50) NIK atau ATM card
	public String LoginNumber; //varchar2(50) nomor KTP atau ATM Card
	public String BranchCode; //varchar2(50) dari device management
	public String TransactionType; //ganti buku, cetak buku.
	public String LogoutReason; //varchar2(50) success, auto logout
}
