package model;

public class mdlChangePassbook {
	public String SerialNumber;
	public String WSID;
	public String BDSVersion;
	public String MPPTransactionCode;
	public String AccountNumber;
	public String BranchCode;
	public String CashDrawer;
	public String PassbookType;
	public String UserID;
	public mdlSmallPassbookInformation SmallPassbookInformation;
}
