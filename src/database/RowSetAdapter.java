package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import org.eclipse.jdt.internal.compiler.ast.ThrowStatement;

import com.sun.rowset.JdbcRowSetImpl;
import com.sun.rowset.RowSetFactoryImpl;

import java.sql.*;
import javax.sql.DataSource;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.*;
import java.io.*;
import oracle.jdbc.OracleConnection;

/** Documentation
 *
 */
public class RowSetAdapter {

	private static Connection conn;

	public static JdbcRowSet getJDBCRowSet() throws Exception{
		InitialContext context = new InitialContext();
    	DataSource dataSource = (DataSource) context.lookup("java:/comp/env/jdbc/gg_merchant");
    	conn = dataSource.getConnection();
    	JdbcRowSetImpl jdbcRs = new JdbcRowSetImpl(conn);

    	return jdbcRs;
	}

	public static Connection getConnection()throws Exception{

			 try {
			     InitialContext context = new InitialContext();
			     DataSource dataSource = (DataSource) context.lookup("java:/comp/env/jdbc/gg_merchant");

			     conn = dataSource.getConnection();

			 }
			 catch (Exception e) {
		            e.printStackTrace();
		     }


		     return conn;
	}

	public static OracleConnection getConnectionWL() {
		 OracleConnection conn=null;
		 javax.sql.DataSource ds=null;
		 
		 try{
		 // Get the base naming context from web.xml
		 Context weblogicparam = (Context)new InitialContext().lookup("java:comp/env");
		 
		// Get a single value from web.xml
		 String urlparam = (String)weblogicparam.lookup("param_provider_url");
		 String datasourceparam = (String)weblogicparam.lookup("param_datasource");
		   
		    Hashtable env = new Hashtable();
		    env.put( Context.INITIAL_CONTEXT_FACTORY, "weblogic.jndi.WLInitialContextFactory" );
		    //env.put(Context.PROVIDER_URL, "t3://localhost:7001");
		    env.put(Context.PROVIDER_URL, urlparam);

		    //try{
		      Context context=new InitialContext( env );
		      //you will need to have create a Data Source with JNDI name testDS
		      //ds=(javax.sql.DataSource) context.lookup ("jdbc/bca");
		      ds=(javax.sql.DataSource) context.lookup (datasourceparam);
		      conn=(OracleConnection) ds.getConnection();
		      java.util.Properties prop = new java.util.Properties();
		      System.out.println("Connection object details : " + conn);
		      //conn.close();
		}
		catch(Exception ex){
		      //handle the exception
		      ex.printStackTrace();
		 }

	   return conn;
	}

	
}
